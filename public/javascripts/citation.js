(function (root, document, factory) {
  if (typeof define === "function" && define.amd) define(factory);
  else root.Citation = factory();
})(window, document, function () {
  
  // Static vars
  var citationCount = 0;
  var colors = {
    primary: new PSPDFKit.Color({
      r: 186,
      g: 0,
      b: 0,
    }),
    secondary: new PSPDFKit.Color({
      r: 204,
      g: 97,
      b: 10,
    }),
  }

  return function Citation(instance, match, searchResult) {

    function addNewCitation(highlight, match) {
      var page = instance.pageInfoForIndex(highlight.pageIndex);
      var id = citationCount++;

      let container = document.createElement('div');
      container.classList.add('citation');
      container.innerHTML = `
        <h4 class="citation__title citation__title--${match.category}">
          ${match.title}
        </h4>
        <ul class="citation__item-list">
          <li class="citation__item">Regulation: ${match.regulation}</li>
          <li class="citation__item">Topic: ${match.topic}</li>
          <li class="citation__item">Sub Topic: ${match.subtopic}</li>
        </ul>`;

      const item = new PSPDFKit.CustomOverlayItem({
        id: `citation-${id}`,
        node: container,
        pageIndex: highlight.pageIndex,
        position: new PSPDFKit.Geometry.Point({
          x: page.width + 20 + (272 * Math.floor(id / 6)),
          y: 20 + (120 * (id % 6)),
        }),
        onAppear() {
          console.log('rendered!');
        }
      });
      console.log('item', item)
      instance.setCustomOverlayItem(item);
      return item;
    }

    function newHighlightAnnotation(searchResult) {
      return new PSPDFKit.Annotations.HighlightAnnotation({
        pageIndex: searchResult.pageIndex,
        rects: searchResult.rectsOnPage,
        boundingBox: PSPDFKit.Geometry.Rect.union(searchResult.rectsOnPage),
      });
    }

    function newRectangleAnnotation(searchResult) {
      return new PSPDFKit.Annotations.RectangleAnnotation({
        pageIndex: searchResult.pageIndex,
        boundingBox: PSPDFKit.Geometry.Rect.union(searchResult.rectsOnPage),
        strokeColor: colors[match.category],
        strokeWidth: 1,
      });
    }

    function addAnnotationToDocument(annotation) {
      return instance
        .createAnnotation(annotation)
        .then(instance.ensureAnnotationSaved)
    }

    function newArrowAnnotation(searchResult, from, to) {
      var start = new PSPDFKit.Geometry.Point({ x: from.boundingBox.left + from.boundingBox.width, y: from.boundingBox.top + (from.boundingBox.height / 2) });
      var end = new PSPDFKit.Geometry.Point({ x: to.position.x + 5, y: to.position.y + 20 });

      return new PSPDFKit.Annotations.LineAnnotation({
        pageIndex: searchResult.pageIndex,
        startPoint: start,
        endPoint: end,
        boundingBox: new PSPDFKit.Geometry.Rect({
          left: start.x < end.x ? start.x : end.x, // ?
          top: start.y < end.y ? start.y : end.y, // ?
          width: Math.abs(start.x - end.x) + 10, // ?
          height: Math.abs(start.y - end.y) + 10, // ?
        }),
        strokeColor: colors[match.category],
        strokeWidth: 1,
      })
    }

    let rectAnno = newRectangleAnnotation(searchResult);
    let details = addNewCitation(rectAnno, match);
    let arrow = newArrowAnnotation(searchResult, rectAnno, details);

    return Promise.all([
      addAnnotationToDocument(rectAnno),
      addAnnotationToDocument(arrow),
    ])
  }
})